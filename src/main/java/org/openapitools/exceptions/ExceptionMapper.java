package org.openapitools.exceptions;


import org.openapitools.exceptions.*;
import org.openapitools.model.ResponseHeader;
import org.openapitools.model.Error;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.time.LocalDateTime;

import java.time.OffsetDateTime;
import java.util.UUID;

@ControllerAdvice
public class ExceptionMapper {

    @ExceptionHandler({UserAlreadyExists.class})
    public ResponseEntity<Error> handleBusinessException() {
        return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(new Error()
                .responseHeader(new ResponseHeader()
                        .requestId(UUID.randomUUID())
                        .sendDate(OffsetDateTime.now()))
                .code("ALREADY_EXISTS")
                .message("User already exists"));
    }
    @ExceptionHandler({NotFoundException.class})
    public ResponseEntity<Error> handleNotFoundException(NotFoundException ex) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new Error()
                .responseHeader(new ResponseHeader()
                        .requestId(UUID.randomUUID())
                        .sendDate(OffsetDateTime.now()))
                .code("NOT_FOUND")
                .message("Resource doesn't exist"));
    }

//    @ExceptionHandler({BadRequestExcep.class})
//    public ResponseEntity<Error> handleBadRequest(BadRequestExcep ex) {
//        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Error()
//                .responseHeader(new ResponseHeader()
//                        .requestId(UUID.randomUUID())
//                        .sendDate(OffsetDateTime.now()))
//                .code("BAD_REQUEST")
//                .message("400 Bad request"));
//    }

    @ExceptionHandler({UnauthorizedException.class})
    public ResponseEntity<Error> handleUnauthorizedException(UnauthorizedException ex) {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new Error()
                .responseHeader(new ResponseHeader()
                        .requestId(UUID.randomUUID())
                        .sendDate(OffsetDateTime.now()))
                .code("UNAUTHORIZED_ACCESS")
                .message("Incorrect username or password."));
    }

    @ExceptionHandler({MethodArgumentNotValidException.class, IllegalStateException.class})
    public ResponseEntity<Error> handleException(MethodArgumentNotValidException ex) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Error()
                .responseHeader(new ResponseHeader()
                        .requestId(UUID.randomUUID())
                        .sendDate(OffsetDateTime.now()))
                .code("BAD_REQUEST")
                .message("Bad request."));
    }

    @ExceptionHandler({InvalidHmacSignatureException.class})
    public ResponseEntity<Error> handleInvalidHmacSignatureException(InvalidHmacSignatureException ex){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Error()
                .responseHeader(new ResponseHeader()
                        .requestId(UUID.randomUUID())
                        .sendDate(OffsetDateTime.now()))
                .code("INVALID_SIGNATURE")
                .message("Invalid HMAC signature."));
    }


}

