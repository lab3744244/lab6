package org.openapitools.interceptor;


import org.apache.commons.codec.binary.Hex;
import org.openapitools.exceptions.InvalidHmacSignatureException;
import org.springframework.web.servlet.HandlerInterceptor;
import org.openapitools.interceptor.MultiReadHttpServletRequest;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class RawRequestInterceptor implements HandlerInterceptor {

    private static MultiReadHttpServletRequest requestWrapper;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        requestWrapper = new MultiReadHttpServletRequest(request);
        return true;
    }

    public static String generateHmac(String algorithm, String data, String key)
            throws NoSuchAlgorithmException, InvalidKeyException {

        SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes(), algorithm);
        Mac mac = Mac.getInstance(algorithm);
        mac.init(secretKeySpec);

        return Hex.encodeHexString(mac.doFinal(data.getBytes()));
    }

    public static void verifyHmacSignature(String secretKey, String hmacSignature)
            throws NoSuchAlgorithmException, InvalidKeyException {

        String hexS = generateHmac("HmacSHA256", requestWrapper.getRequestBody(), secretKey);
        if (!hexS.equals(hmacSignature) || hmacSignature.isEmpty()) {
            throw new InvalidHmacSignatureException();
        }
    }
}

